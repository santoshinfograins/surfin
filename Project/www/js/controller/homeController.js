surfin.controller('homeController', function($scope, $rootScope, $http, $ionicLoading, $state, URL){
	$rootScope.type = {};
	$scope.teacherCources = [];
	$scope.course = {};
	if($rootScope.isRequested == undefined){
    $rootScope.isRequested = true  
  }
	
	$scope.search = function(){
		$ionicLoading.show();
		var data = {"type" : $scope.type.name}

		$http({
			method 	: "POST",
			url 		: URL.baseDomain + "/search",
			data		: data 
		})
		.success(function(success){
			$scope.searchResult = success;
			$ionicLoading.hide();
		})
		.error(function(error){
			console.log(error)
			$ionicLoading.hide();
		})
	}

	$scope.slectedProfile = function(user){
		$ionicLoading.show()
		$rootScope.selectedUser = user;
		if($rootScope.selectedUser.type == 'Teacher'){
		  var data = {'teacherId' : $rootScope.selectedUser._id}
		  $http({
		  	method : 'POST',
		  	url : URL.baseDomain + '/allrating',
		  	data : data 
		  })
		  .success(function(success){
		  	$rootScope.groups = [];
		  	var rateArray = [];
		  	for(var i in success.rating){
	  			$rootScope.groups[i] = {
			      name:success.rating[i].subject,
			      items: [],
			      show: false
			    }	
		  	}
		  	
		  	for(var i in success.rating){
		  		for(var j in success.rating[i].rate){
		  			$rootScope.groups[i].items.push({'student' : success.rating[i].rate[j].student, 'rate' : success.rating[i].rate[j].rate})
		  		}
		  	}
		  	for(var i in $rootScope.groups){
		  		for( var j in $rootScope.groups[i].items){
		  			if($rootScope.groups[i].items[j].rate){
			  			for(var k in $rootScope.groups[i].items[j].rate){
			  				if($rootScope.groups[i].items[j].rate[k]){
				  				$rootScope.groups[i].items[j].rate[k] = 1				  			}
				  			else{
				  				$rootScope.groups[i].items[j].rate[k] = 0
				  			}
				  		}
			  		}
		  		}
		  	}
		  	$http({
          method : 'POST',
          url : URL.baseDomain + '/getspecificrequest',
          data : {requestedById : $scope.loggedInUser._id, requestedForId : $scope.selectedUser._id}
        })
        .success(function(success){
          if(success.length){
	          $rootScope.getRequestInfo = success[success.length-1];
	          $rootScope.getRequestInfo.response = parseInt($rootScope.getRequestInfo.response)
	          if($rootScope.getRequestInfo.response){
	          	
	          }
	          else{
	          	$rootScope.isRequested = true;
	          }
          }
          else{
          	$rootScope.isRequested = false;
          }
        })
        .error(function(error){
          console.log(error)
        })
		  	$ionicLoading.hide();
		  })
		  .error(function(error){
		  	$ionicLoading.hide();
		  	console.log(error)
		  })
		}
		else{
			$ionicLoading.hide();
			$rootScope.groups = [];
		}
		$state.go('detail');
	}

	$scope.getCourses = function(){
		$ionicLoading.show();
		var data = {'id' : $rootScope.loggedInUser._id}
		
		$http({
			method : 'POST',
			url : URL.baseDomain + "/courses",
			data : data
		})
		.success(function(success){
			$ionicLoading.hide();
			$rootScope.courses = success;
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error);
		})
	}

	$scope.addCourse = function(){
		$state.go('addCourse')
	}

	$scope.submitCourse = function(){
		$ionicLoading.show();
		var data = {'userId' : $rootScope.loggedInUser._id, 'courseName' : $scope.course.name}
		$http({
			method : 'POST',
			url : URL.baseDomain + '/addcourse',
			data : data
		})
		.success(function(success){
			$ionicLoading.hide();
			$state.go('teacherHome')
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}
})