surfin.controller("authController", function($scope, $rootScope, $http, $state,	$ionicLoading, $translate, URL){
	
	$scope.forgetpassworderror = false;
	$scope.user = {};
	// $scope.user.email = 'nicolas@yopmail.com'
	// $scope.user.password = '123456'
	$scope.newUser = {};
	$scope.oldUser = {};
	$rootScope.loggedInUser = {};
	$scope.position = {};
	$ionicLoading.hide();
	$scope.matchPassword = false;
 	
 	$scope.emailFormat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if($scope.newUser.type == undefined){
		$scope.newUser.type = 'Teacher'
	}

	$scope.login = function(){
		$ionicLoading.show();
		var data = {"email" : $scope.user.email, "password" : $scope.user.password}

		$http({
			method 	: "POST",
			url 		: URL.baseDomain + "/login",
			data		: data 
		})
		.success(function(success){
			$ionicLoading.hide();
			if(success.status){
				$rootScope.loggedInUser = success;
				// alert(success.message)
				if($rootScope.loggedInUser.image == undefined){
					$rootScope.loggedInUser.image = 'img/images.png'
				} 
				var userStarRating = [];
				$rootScope.loggedInUser.userStarRating = userStarRating
				if(success.type == "Student"){
					$state.go('studentHome')	
				}
				else{
					$state.go('teacherHome')
				}
			}
			else{
				alert(success.message)
				$scope.user = {};
				$state.go('login')
			}
		})
		.error(function(error){
			$ionicLoading.hide();
			alert(error)
			console.log(error)
		})
	}

	$scope.forgetPassword = function(){
		$ionicLoading.show();
		var data = {'email' : $scope.oldUser.email}

		$http({
			method : 'POST',
			url 	 : URL.baseDomain + "/getpassword",
			data 	 : data
		})
		.success(function(success){
			$ionicLoading.hide();
			if(success.status){
				alert($translate.instant('forget_pasword_msg'))
				$state.go('login')
			}
			else{
				$scope.forgetpassworderror = true;
				$scope.forgetpasswordmessage = success.message;
			}
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error);
		})
	}

	$rootScope.logout = function(){
		$rootScope.loggedInUser = {};
		$state.go('login');
	}

	$scope.signup = function(){
    $ionicLoading.show();
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;//January is 0, so always add + 1

		var yyyy = today.getFullYear();
		if(dd<10){dd='0'+dd}
		if(mm<10){mm='0'+mm}
		today = mm+'/'+dd+'/'+yyyy;
		var array = [0,0,0,0,0]
		var data = {
			"firstname" : $scope.newUser.firstname,
			"lastname"  : $scope.newUser.lastname, 
			"username"  : $scope.newUser.username, 
			"useremail" : $scope.newUser.email , 
			"type" 			: $scope.newUser.type, 
			"password" 	: $scope.newUser.password,
			"createdon" : today,
			"image" 		: 'img/images.png',
			"rating" : array
			// "latitude"  : $scope.position.coords.latitude,
			// "longitude"  : $scope.position.coords.longitude,
		}
		$http({
			method 	: "POST",
			url 		: URL.baseDomain + "/signup",
			data		: data 
		})
		.success(function(success){
			$ionicLoading.hide();
			$rootScope.loggedInUser = success
				$state.go('profile')
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)

		})
	}

	// $scope.blur = function(){
		
	// 	if($scope.newUser.password == $scope.newUser.confirmPassword){
	// 		$scope.matchPassword = true;
	// 		alert($scope.matchPassword)
	// 	}
	// 	else{
	// 		$scope.matchPassword = false;
	// 		alert($scope.matchPassword)
	// 	}
	// }
	// $scope.test = function(){
	// 	alert(123)
	// 	var onSuccess = function(position) {
	//     $scope.position = position
	//     alert('Latitude: ' + position.coords.latitude + '\n' + 'Longitude: ' + position.coords.longitude + '\n');
 //    }

 //    function onError(error) {
 //      alert('code: '    + error.code    + '\n' +
 //        'message: ' + error.message + '\n');
 //    }

 //    navigator.geolocation.getCurrentPosition(onSuccess, onError);
	// }
})