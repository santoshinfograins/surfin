surfin.controller('userController', function($scope, $rootScope, $http, $state, $location, $ionicLoading, URL, localStorageService, SocketService, $cordovaCamera, $cordovaImagePicker, $cordovaDialogs, $cordovaFileTransfer){
	
  if($scope.actitudAbility == undefined ){
    $scope.actitudAbility = [
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white'
    ];  
  }

  if($scope.forceSkill == undefined ){
    $scope.forceSkill = [
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white'
    ];  
  }
  
  if($scope.puntuality == undefined ){
    $scope.puntuality = [
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white'
    ];  
  }

  if($scope.patience == undefined ){
    $scope.patience = [
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white'
    ];  
  }
  
  if($scope.explanation == undefined ){
    $scope.explanation = [
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white'
    ];  
  }
  
  if($scope.punctual == undefined ){
    $scope.punctual= [
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white'
    ];  
  }
  
  if($scope.ability == undefined ){
    $scope.ability = [
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white',
    'ion-android-star-outline color_white'
    ];  
  }

	if($rootScope.selectedUser == undefined){
		$rootScope.selectedUser = [];	
	}
	
	$rootScope.searchedUser = {}
  $scope.teacher = {}
	$ionicLoading.hide();
	$scope.disabledText = true;
	$scope.disabledTick = true;
	$scope.disabledPencil = false;
  if($rootScope.isRequested == undefined){
    $rootScope.isRequested = ''  
  }
  $scope.response = {}

	$scope.updateProfile = function(){
    $ionicLoading.show()
    $http({
      method : 'POST',
      url : URL.baseDomain + '/updateProfile',
      data : $rootScope.loggedInUser
    })
    .success(function(success){
      $ionicLoading.hide()
      if($rootScope.loggedInUser.type == 'Teacher'){
        $state.go('teacherHome')
      }
      else{
        $state.go('studentHome')
      }
    })
    .error(function(error){
      $ionicLoading.hide()
      alert(JSON.stringify(error))
    })
  }

  $scope.search = function(){
		$ionicLoading.show();
		var data = {"type" : $scope.type.name}

		$http({
			method 	: "POST",
			url 		: URL.baseDomain + "/search",
			data		: data 
		})
		.success(function(success){
			$scope.searchResult = success;
			$ionicLoading.hide();
		})
		.error(function(error){
			console.log(error)
			$ionicLoading.hide();
		})
	}

	$scope.detail = function(user){
		$ionicLoading.show();
		$rootScope.searchedUser = user;
		$state.go('searchedUserProfile')
	}

	$scope.slectedProfile = function(user){
		$ionicLoading.show();
    $rootScope.selectedUser = user;
    if($rootScope.selectedUser.type == 'Teacher'){
		  var data = {'teacherId' : $rootScope.selectedUser._id}
		  $http({
		  	method : 'POST',
		  	url : URL.baseDomain + '/allrating',
		  	data : data 
		  })
		  .success(function(success){
		  	
		  	$rootScope.groups = [];
		  	var rateArray = [];
		  	for(var i in success.rating){
	  			$rootScope.groups[i] = {
			      name:success.rating[i].subject,
			      items: [],
			      show: false
			    }	
		  	}
		  	
		  	for(var i in success.rating){
		  		for(var j in success.rating[i].rate){
		  			$rootScope.groups[i].items.push({'student' : success.rating[i].rate[j].student, 'rate' : success.rating[i].rate[j].rate})
		  		}
		  	}

		  	for(var i in $rootScope.groups){
		  		for( var j in $rootScope.groups[i].items){
		  			if($rootScope.groups[i].items[j].rate){
			  			for(var k in $rootScope.groups[i].items[j].rate){
			  				if($rootScope.groups[i].items[j].rate[k]){
				  				$rootScope.groups[i].items[j].rate[k] = 1
				  			}
				  			else{
				  				$rootScope.groups[i].items[j].rate[k] = 0
				  			}
				  		}
			  		}
		  		}
		  	}
        $http({
          method : 'POST',
          url : URL.baseDomain + '/getspecificrequest',
          data : {requestedById : $scope.loggedInUser._id, requestedForId : $scope.selectedUser._id}
        })
        .success(function(success){
          if(success.length){
            $rootScope.isRequested = true;
            // $scope.requested = true;
            $scope.getRequestInfo = success;
            // if($scope.request.response){
            //  $scope.requested = false
            // }
          }
          else{
            $rootScope.isRequested = false;
          }
        })
        .error(function(error){
          console.log(error)
        })
        $ionicLoading.hide();
		  })
		  .error(function(error){
		  	$ionicLoading.hide();
        console.log(error)
		  })
      
		}
		else{
			$ionicLoading.hide();
      $rootScope.groups = [];
		}
		$state.go('detail');
	}

	$scope.editProfile = function(){
		$scope.disabledText = false;
		$scope.disabledTick = false;
		$scope.disabledPencil = true;
	}

	$scope.saveChanges = function(){
		$scope.disabledText = true;
		$scope.disabledTick = true;
		$scope.disabledPencil = false;
	}
  
  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    group.show = !group.show;
  };
  
  $scope.isGroupShown = function(group) {
    return group.show;
  };

  $scope.current_room = localStorageService.get('room');
  
  $scope.startChat = function(username) {
    localStorageService.set('username', username);
    var temp
    if($rootScope.loggedInUser.type=='Teacher'){
      temp = $rootScope.loggedInUser.username+'_'+$rootScope.selectedUser.username
    }
    else if($rootScope.selectedUser.type=='Teacher'){
      temp = $rootScope.selectedUser.username+'_'+$rootScope.loggedInUser.username
    }
    
    localStorageService.set('room', temp);

    var room = {
      'room_name': temp
    };

    SocketService.emit('join:room', room);

    $state.go('chat');
  };

  $scope.selectStarActitudAbility = function(className, index){
  	if(index == 0){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.actitudAbility[i] = 'ion-android-star-outline color_white'	
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.actitudAbility[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 1){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.actitudAbility[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.actitudAbility[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 2){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.actitudAbility[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.actitudAbility[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 3){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.actitudAbility[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.actitudAbility[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else{
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.actitudAbility[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.actitudAbility[i] = 'ion-android-star gold'
  			}
  		}
  	}
  }

  $scope.selectStarForceSkill = function(className, index){
  	if(index == 0){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.forceSkill[i] = 'ion-android-star-outline color_white'	
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.forceSkill[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 1){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.forceSkill[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.forceSkill[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 2){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.forceSkill[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.forceSkill[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 3){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.forceSkill[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.forceSkill[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else{
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.forceSkill[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.forceSkill[i] = 'ion-android-star gold'
  			}
  		}
  	}
  }

  $scope.selectStarPuntuality = function(className, index){
  	if(index == 0){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.puntuality[i] = 'ion-android-star-outline color_white'	
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.puntuality[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 1){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.puntuality[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.puntuality[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 2){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.puntuality[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.puntuality[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 3){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.puntuality[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.puntuality[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else{
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.puntuality[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.puntuality[i] = 'ion-android-star gold'
  			}
  		}
  	}
  }

  $scope.selectStarPatience = function(className, index){
  	if(index == 0){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.patience[i] = 'ion-android-star-outline color_white'	
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.patience[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 1){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.patience[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.patience[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 2){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.patience[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.patience[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 3){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.patience[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.patience[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else{
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.patience[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.patience[i] = 'ion-android-star gold'
  			}
  		}
  	}
  }
  
  $scope.selectStarExplanation = function(className, index){
  	if(index == 0){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.explanation[i] = 'ion-android-star-outline color_white'	
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.explanation[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 1){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.explanation[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.explanation[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 2){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.explanation[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.explanation[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 3){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.explanation[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.explanation[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else{
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.explanation[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.explanation[i] = 'ion-android-star gold'
  			}
  		}
  	}
  }
  
  $scope.selectStarPunctual = function(className, index){
  	if(index == 0){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.punctual[i] = 'ion-android-star-outline color_white'	
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.punctual[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 1){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.punctual[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.punctual[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 2){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.punctual[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.punctual[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 3){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.punctual[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.punctual[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else{
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.punctual[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.punctual[i] = 'ion-android-star gold'
  			}
  		}
  	}
  }
  
  $scope.selectStarAbility = function(className, index){
  	if(index == 0){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.ability[i] = 'ion-android-star-outline color_white'	
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.ability[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 1){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.ability[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.ability[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 2){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.ability[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.ability[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else if(index == 3){
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.ability[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.ability[i] = 'ion-android-star gold'
  			}
  		}
  	}
  	else{
  		if(className == 'ion-android-star gold'){
  			for(var i=index+1; i<=4; i++){
  				$scope.ability[i] = 'ion-android-star-outline color_white'
  			}
  		}
  		else{
  			for(var i=0; i<=index; i++ ){
  				$scope.ability[i] = 'ion-android-star gold'
  			}
  		}
  	}
  }

  $scope.rateStudent = function(){
  	$ionicLoading.show()
    var actitudAbilityCount = 0;
  	var forceSkillCount = 0;
  	var puntualityCount = 0;
  	var rate
  	for(var i in $scope.actitudAbility){
  		if($scope.actitudAbility[i] == 'ion-android-star gold'){
  			actitudAbilityCount++;
  		}
  	}
  	for(var i in $scope.forceSkill){
  		if($scope.forceSkill[i] == 'ion-android-star gold'){
  			forceSkillCount++;
  		}
  	}
  	for(var i in $scope.puntuality){
  		if($scope.puntuality[i] == 'ion-android-star gold'){
  			puntualityCount++;
  		}
  	}
  	
  	rate = (actitudAbilityCount+forceSkillCount+puntualityCount)/3
		var data = {
			teacherId : $rootScope.loggedInUser._id,
			teacherName : $rootScope.loggedInUser.username,
			studentId : $rootScope.selectedUser._id,
			studentName : $rootScope.selectedUser.username,
			rating : Math.ceil(rate)
		}

		$http({
			method : 'POST',
			url : URL.baseDomain + '/setStudentRating',
			data : data
		})
		.success(function(success){
      var data = {
        Id : $rootScope.selectedUser._id,
        type : 'Student'
      }
      $http({
        method : 'POST',
        url : URL.baseDomain + '/getrating',
        data : data
      })
      .success(function(success){
        var rate
        var total = 0
        for(var i in success.rating){
          rate = success.rating[i].rate
          for(var j in rate){
            for(var k in rate[j].rate){
              total = total + rate[j].rate[k]
            }
          }
        }
        var data = {
          userId : $rootScope.selectedUser._id,
          firstname : $rootScope.selectedUser.firstname ,
          lastname : $rootScope.selectedUser.lastname ,
          username : $rootScope.selectedUser.username ,
          email : $rootScope.selectedUser.email ,
          password : $rootScope.selectedUser.password,
          type : $rootScope.selectedUser.type ,
          createdon : $rootScope.selectedUser.createdon,
          image : $rootScope.selectedUser.image,
          courses : $rootScope.selectedUser.courses,
          address : $rootScope.selectedUser.address,          
          rating : Math.ceil(total/rate.length)
        }
        $http({
          method: 'POST',
          url : URL.baseDomain + '/mainRating',
          data : data
        })
        .success(function(success){
          $state.go('teacherHome')
        })
        .error(function(error){
          console.log(error)
        })
        
      })
      .error(function(error){
        console.log(error)
        $ionicLoading.hide()  
      })
		})
		.error(function(error){
			$ionicLoading.hide()
      console.log(error)
		})
  }

  $scope.rateTeacher = function(){
  	$ionicLoading.show();
    var patienceCount = 0;
  	var explanationCount = 0;
  	var punctualCount = 0;
  	var abilityCount = 0;
  	var rate
  	for(var i in $scope.actitudAbility){
  		if($scope.patience[i] == 'ion-android-star gold'){
  			patienceCount++;
  		}
  	}
  	for(var i in $scope.forceSkill){
  		if($scope.explanation[i] == 'ion-android-star gold'){
  			explanationCount++;
  		}
  	}
  	for(var i in $scope.puntuality){
  		if($scope.punctual[i] == 'ion-android-star gold'){
  			punctualCount++;
  		}
  	}
		for(var i in $scope.puntuality){
  		if($scope.ability[i] == 'ion-android-star gold'){
  			abilityCount++;
  		}
  	}  	
  	rate = (patienceCount+explanationCount+punctualCount+abilityCount)/4
		var data = {
			teacherId : $rootScope.selectedUser._id,
			teacherName : $rootScope.selectedUser.username,
			studentId : $rootScope.loggedInUser._id,
      studentImage : $rootScope.loggedInUser.image,
			studentName : $rootScope.loggedInUser.username,
			subject : $scope.teacher.course,
			rating : Math.ceil(rate)
		}

		$http({
			method : 'POST',
			url : URL.baseDomain + '/setTeacherRating',
			data : data
		})
		.success(function(success){
			console.log(success)
      var data = {
        Id : $rootScope.selectedUser._id,
        type : 'Teacher'
      }
      $http({
        method : 'POST',
        url : URL.baseDomain + '/getrating',
        data : data
      })
      .success(function(success){
        var rate
        var total = 0
        var length = 0
        for(var i in success.rating){
          rate = success.rating[i].rate
          
          for(var j in rate){
            for(var k in rate[j].rate){
              total = total + rate[j].rate[k]
            }
          }
          length = length + rate.length
        }
        
        var data = {
          userId : $rootScope.selectedUser._id,
          firstname : $rootScope.selectedUser.firstname ,
          lastname : $rootScope.selectedUser.lastname ,
          username : $rootScope.selectedUser.username ,
          email : $rootScope.selectedUser.email ,
          password : $rootScope.selectedUser.password,
          type : $rootScope.selectedUser.type ,
          createdon : $rootScope.selectedUser.createdon,
          image : $rootScope.selectedUser.image,
          courses : $rootScope.selectedUser.courses,
          address : $rootScope.selectedUser.address,          
          rating : Math.ceil(total/length)
        }
        $http({
          method: 'POST',
          url : URL.baseDomain + '/mainRating',
          data : data
        })
        .success(function(success){
          $ionicLoading.hide();
          $state.go('studentHome')
        })
        .error(function(error){
          console.log(error)
        })
      })
      .error(function(error){
        console.log(error)
        $ionicLoading.hide()  
      })
		})
		.error(function(error){
			$ionicLoading.hide();
      console.log(error)
		})
  }

  $scope.request = function(user){
    // $scope.requested = true;
    var data = {
      requestedByName : $rootScope.loggedInUser.username,
      requestedById : $rootScope.loggedInUser._id,
      requestedForName : $rootScope.selectedUser.username,
      requestedForId : $rootScope.selectedUser._id
    }

    $http({
      method : 'POST',
      url : URL.baseDomain + '/sendrequest',
      data : data
    })
    .success(function(success){
      $rootScope.isRequested = true;
    })
    .error(function(error){
      console.log(error)
    })
  }

  $scope.getRequest = function(){
    $http({
      method : 'POST',
      url : URL.baseDomain + '/getallrequest',
      data : {requestedForId : $rootScope.loggedInUser._id }
    })
    .success(function(success){
      $scope.requests = success
    })
    .error(function(error){
      console.log(error)
    })
  }

  // $scope.getDateTime = function(){
  //   var options = {
  //     date: new Date(),
  //     mode: 'date'
  //   };

  //   function onSuccess(date) {
  //     alert('Selected date: ' + date);
  //   }

  //   function onError(error) { // Android only
  //     alert('Error: ' + error);
  //   }

  //   datePicker.show(options, onSuccess, onError);
  // }

  $scope.updateResponse = function(request){
    var data = {
      id : request._id,
      requestedByName : request.requestedByName,
      requestedById : request.requestedById,
      requestedForName : request.requestedForName,
      requestedForId : request.requestedForId,
      response : $scope.response.status,
    }
    $http({
      method : 'POST',
      url : URL.baseDomain + '/updaterequest',
      data : data
    })
    .success(function(success){

    })
    .error(function(error){
      console.log(error)
    })
  }

  $scope.uploadImage = function(){
    $cordovaDialogs.confirm('Upload profile Image From', 'Profile Image', ['Camera','Gallery'])
    .then(function(buttonIndex) {
      if(buttonIndex == 1){
        $scope.openCamera()
      }
      else{
      $scope.openGallery()
      }  // no button = 0, 'OK' = 1, 'Cancel' = 2
      // var btnIndex = buttonIndex;
    });    
  }

  $scope.openCamera = function(){
    // var options = {
    //   quality: 50,
    //   destinationType: Camera.DestinationType.DATA_URL,
    //   sourceType: Camera.PictureSourceType.CAMERA,
    //   allowEdit: true,
    //   encodingType: Camera.EncodingType.JPEG,
    //   targetWidth: 100,
    //   targetHeight: 100,
    //   popoverOptions: CameraPopoverOptions,
    //   saveToPhotoAlbum: false,
    // correctOrientation:true
    // };

    // $cordovaCamera.getPicture(options).then(function(imageData) {
    //   // var image = document.getElementById('myImage');
    //   // image.src = "data:image/jpeg;base64," + imageData;
      
    //   $rootScope.loggedInUser.image = "data:image/jpeg;base64," + imageData;
    //   alert(imageData)
    // }, function(err) {
    //   // error
    // });
    navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
      destinationType: Camera.DestinationType.FILE_URI });

    function onSuccess(imageURI) {
      $rootScope.loggedInUser.image = imageURI;
      var server = URL.baseDomain + '/upload';
      var optionsNew = new FileUploadOptions();
      optionsNew.fileKey = "file";
      optionsNew.fileName = $rootScope.loggedInUser.image.substr($rootScope.loggedInUser.image.lastIndexOf('/') + 1);
      optionsNew.enctype = "multipart";

      var params = {};
      params.value1 = "test";
      params.value2 = "param";

      optionsNew.params = params
      
      $cordovaFileTransfer.upload(server, $rootScope.loggedInUser.image, optionsNew)
        .then(function(result) {
          $ionicLoading.hide()
          $rootScope.loggedInUser.image = URL.baseDomain+'/'+result.response
        }, function(error) {
          $ionicLoading.hide()
          // alert('error'+ JSON.stringify(error))
        }, function (progress) {
          $ionicLoading.show()
          // alert('progress'+ JSON.stringify(progress))
        });
    }

    function onFail(message) {
      alert('Failed because: ' + message);
    }
  }

  $scope.openGallery = function(){
    // $ionicLoading.show()
    var options = {
      maximumImagesCount: 1,
      width: 800,
      height: 800,
      quality: 80
    };

    $cordovaImagePicker.getPictures(options)
      .then(function (results) {
        $rootScope.loggedInUser.image = results[0]
        var server = URL.baseDomain + '/upload';
        var optionsNew = new FileUploadOptions();
        optionsNew.fileKey = "file";
        optionsNew.fileName = $rootScope.loggedInUser.image.substr($rootScope.loggedInUser.image.lastIndexOf('/') + 1);
        optionsNew.mimeType = "text/plain";

        var params = {};
        params.value1 = "test";
        params.value2 = "param";

        optionsNew.params = params

        $cordovaFileTransfer.upload(server, $rootScope.loggedInUser.image, optionsNew)
        .then(function(result) {
          $ionicLoading.hide()
          $rootScope.loggedInUser.image = URL.baseDomain+'/'+result.response
        }, function(error) {
          $ionicLoading.hide()
          // alert('error'+ JSON.stringify(error))
        }, function (progress) {
          $ionicLoading.show()
          // alert('progress'+ JSON.stringify(progress))
        });
      }, function(error) {
        $ionicLoading.hide()
        alert(error)
      });
  }

  var geocoder
  
  $scope.getLocation = function(){
    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);

    //Get the latitude and the longitude;
    function successFunction(position) {
      var lat = position.coords.latitude;
      var lng = position.coords.longitude;
      geocoder = new google.maps.Geocoder();
      codeLatLng(lat, lng)
    }

    function errorFunction(){
      alert("Geocoder failed");
    }

    function codeLatLng(lat, lng) {
      var latlng = new google.maps.LatLng(lat, lng);
      geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[1]) {
            $rootScope.loggedInUser.address = results[0].formatted_address
            for (var i=0; i<results[0].address_components.length; i++) {
              for (var b=0;b<results[0].address_components[i].types.length;b++) {
                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                  city= results[0].address_components[i];
                  break;
                }
              }
            }
          } 
          else {
            alert("No results found");
          }
        } 
        else {
          alert("Geocoder failed due to: " + status);
        }
      })
    }
    geocoder = new google.maps.Geocoder();
  }

})