
var surfin = angular.module('starter', ['ionic', 'pascalprecht.translate', 'LocalStorageModule', 'btford.socket-io', 'angularMoment', 'timer', 'ngCordova'])

surfin.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

surfin.config(function($stateProvider, $urlRouterProvider) {
    
  $urlRouterProvider.otherwise('/login');
    
  $stateProvider
      
    .state('default', {
      abstract: true,
      views: {
        layout: {
          templateUrl: 'template/layout/defaultLayout.html',
        }
      }
    })
      
      // LOGIN VIEW ========================================
      .state('login', {
        cache: false,
        url: '/login',
        templateUrl: 'template/auth/login.html',
        controller: 'authController',
        parent: 'default',
      })
      
      // SIGNUP VIEW =================================
      .state('signup', {
        cache: false,
        url: '/signup',
        templateUrl: 'template/auth/signup.html',
        controller: 'authController',
        parent: 'default',       
      })

      // SIGNUP VIEW =================================
      .state('forgotPassword', {
        cache: false,
        url: '/forgotPassword',
        templateUrl: 'template/auth/forgotPassword.html',
        controller: 'authController',
        parent: 'default',       
      })
      
    // SIDEMENU VIEW =================================
    .state('sidemenu', {
      abstract: true,
      views: {
        layout: {
          templateUrl: 'template/layout/sidemenuLayout.html',
        }
      }
    })

      // PROFILE VIEW =================================
      .state('profile', {
        cache: false,
        url: '/profile',
        templateUrl: 'template/user/profile.html',
        controller: 'userController',
        parent : 'sidemenu'       
      })

      // STUDENT HOMEPAGE VIEW =================================
      .state('studentHome', {
        cache: false,
        url: '/studentHome',
        templateUrl: 'template/home/studentHome.html',
        controller: 'homeController',
        parent : 'sidemenu'       
      })

      // TEACHER HOMEPAGE VIEW =================================
      .state('teacherHome', {
        cache: false,
        url: '/teacherHome',
        templateUrl: 'template/home/teacherHome.html',
        controller: 'homeController',
        parent : 'sidemenu'       
      })
      
      // PROFILE VIEW =================================
      .state('search', {
        cache: false,
        url: '/search',
        templateUrl: 'template/home/search.html',
        controller: 'userController',
        parent : 'sidemenu'       
      })

      // SEARCHED PROFILE VIEW =================================
      .state('searchedUserProfile', {
        cache: false,
        url: '/searchedUserProfile',
        templateUrl: 'template/home/searchedUserProfile.html',
        controller: 'userController',
        parent : 'sidemenu'       
      })
      
      // PROFILE DETAIL VIEW =================================
      .state('detail', {
        cache: false,
        url: '/detail',
        templateUrl: 'template/user/detail.html',
        controller: 'userController',
        parent : 'sidemenu'       
      })
      
      // CHANGE PASSWORD VIEW =================================

      .state('changePassword',{
        cache: false,
        url: '/changePassword',
        templateUrl: 'template/auth/passwordChange.html',
        parent: 'sidemenu'
      })
      
      //ADD COURSES CHAT VIEW =================================

      .state('addCourse',{
        cache: false,
        url : '/addCOurce',
        templateUrl : 'template/home/addCourse.html',
        controller : 'homeController',
        parent : 'sidemenu'
      })

      // .state('rooms', {
      //   url: '/rooms',
      //   templateUrl: 'templates/rooms.html'
      // })

      .state('chat', {
        cache : false,
        url: '/room',
        templateUrl: 'template/room.html',
        parent: 'sidemenu'
      })

      .state('rateTeacher',{
        cache : false,
        url: '/rateTeacher',
        templateUrl: 'template/user/rateTeacher.html',
        controller : 'userController',
        parent: 'sidemenu'
      })

      .state('rateStudent',{
        cache : false,
        url: '/rateStudent',
        templateUrl: 'template/user/rateStudent.html',
        controller : 'userController',
        parent: 'sidemenu'
      })
      .state('scheduleClass',{
        cache : false,
        url: '/scheduleClass',
        templateUrl: 'template/user/scheduleClass.html',
        controller : 'userController',
        parent: 'sidemenu'
      })
      
})

surfin.constant('URL', {
  baseDomain:'https://surfinserver.herokuapp.com',
  chatServerDomain:'https://newsocketchatapp.herokuapp.com',
  // baseDomain:'http://192.168.1.138:3000',
});
