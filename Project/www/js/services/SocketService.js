(function(){

  angular.module('starter')
  .service('SocketService', ['socketFactory', 'URL', SocketService]);

  function SocketService(socketFactory, URL){
    return socketFactory({

    ioSocket: io.connect(URL.chatServerDomain)

    });
  }
})();