surfin.config(['$translateProvider', function ($translateProvider) {
  $translateProvider.useSanitizeValueStrategy('escapeParameters');
  
  $translateProvider.translations('en', {
    /*Side Menu*/
	    'menu' : 'Menu',
			'search' : 'Search',
			'schedule_class' : 'Schedule Class',
			'chat' : 'Chat',
			'logout' : 'Logout',
			'home' : 'Home',
			'courses' : 'Courses',
			'edit_profile' : 'Edit Profile',
			'profile' : 'Profile',

    /*Login Page*/
	    'login' : 'Login',
	    'forgot_password' : 'Forgot Password',
	    'signup' : 'Don’t have account ! Sign up now',
	    'email' : 'Email',
	    'password' : 'Password',
	    'email_required_error' : 'Email is required',
	    'email_type_error' : 'Invalid Email',
	    'password_required_error' : 'Password is required',
    
    /*Signup Page*/
	    'first_name' : 'First Name',
			'last_name' : 'Last Name',
			'user_name' : 'User Name',
			'password' : 'Password',
			'confirm_password' : 'Confirm Password',
			'sign_up' : 'Sign Up',
			'first_name_required_error' : 'First Name is required',
			'user_name_required_error' : 'User Name is required',
			'last_name_required_error' : 'Last Name is required',
			'confirm_password_required_error' : 'Confirm password is required',
			'confirm_password_mismatch' : 'Password should be same',
			'Teacher' : 'Teacher',
			'Student' : 'Student',

		/*Forget password Page*/
			'forget_pasword_msg' : 'Password has been send to your email id',

		/*Teacher Home Page*/
			'teacher' : 'Teacher',
			'student' : 'Student',
			'search' : 'Search',
			'Courses' : 'Courses',
			'Add New Course' : 'Add New Course',

		/*Searched User Profile*/
			'type' : 'Type',
			'connect' : 'Connect',

		/*Teacher's Home*/
			'save' : 'Save',

		/*password change*/
			'Old Password' : 'Old Password',
			'New Password' : 'New Password',
			'Confirm Password' : 'Confirm Password',
			'submit' : 'Submit',

		/*profile*/
			'Email Address' : 'Email Address',
			'Address' : 'Address',
			'CHANGE PASSWORD' : 'CHANGE PASSWORD',

		/*rate student*/
			'Actitud_Ability' : 'Aptitude Ability',
			'Force_Skill' : 'Force Skill',
			'Puntuality' : 'Punctuality',

		/*rate teacher*/
			'Subject' : 'Subject',
			'Patience' : 'Patience',
			'Explanation' : 'Explanation',
			'Punctual' : 'Punctual',
			'Ability' : 'Ability',

		/*schedule class*/
			'Set Time' : 'Set Time',
			'Update' : 'Update',
			'forget' : 'Forget',
			'Change Password' : 'Change Password',
			'Detail' : 'Detail',
			'Rate The Student' : 'Rate The Student',
			'Rate The Teacher' : 'Rate The Teacher',
			'course_required_error' : 'Course name required',
  });

    $translateProvider.translations('spn', {
	    /*Side Menu*/
		    'menu' : 'Menú',
				'search' : 'Buscar',
				'schedule_class' : 'Programar la clase',
				'chat' : 'Charla',
				'logout' : 'Cerrar sesión',
				'home' : 'Casa',
				'courses' : 'Cursos',
				'edit_profile' : 'Editar perfil',
				'profile' : 'Perfil',

	    /*Login Page*/
		    'login' : 'Iniciar sesión',
		    'forgot_password' : 'Se te olvidó tu contraseña',
		    'signup' : 'No tiene cuenta! Regístrese ahora',
		    'email' : 'Email',
		    'password' : 'Contraseña',
		    'email_required_error' : 'correo electronico es requerido',
		    'email_type_error' : 'Email inválido',
		    'password_required_error' : 'se requiere contraseña',
	    
	    /*Signup Page*/
		    'first_name' : 'Nombre de pila',
				'last_name' : 'Apellido',
				'user_name' : 'Nombre de usuario',
				'confirm_password' : 'Confirmar contraseña',
				'sign_up' : 'Regístrate',
				'first_name_required_error' : 'Se requiere el primer nombre',
				'last_name_required_error' : 'Apellido obligatorio',
				'user_name_required_error' : 'Se requiere nombre de usuario',
				'confirm_password_required_error' : 'Confirmar contraseña es obligatoria',
				'confirm_password_mismatch' : 'La contraseña debe ser la misma',
				'Teacher' : 'Profesor',
				'Student' : 'Estudiante',

			/*Forget password Page*/
				'forget_pasword_msg' : 'La contraseña se ha enviado a tu ID de correo electrónico',

			/*Teacher Home Page*/
				'teacher' : 'Profesor',
				'student' : 'Estudiante',
				'search' : 'Buscar',
				'Courses' : 'Cursos',
				'Add New Course' : 'Añadir nuevo curso',

			/*Searched User Profile*/
				'type' : 'Tipo',
				'connect' : 'Conectar',

			/*Teacher's Home*/
				'save' : 'Salvar',

			/*password change*/
				'Old Password' : 'Contraseña anterior',
				'New Password' : 'nueva contraseña',
				'Confirm Password' : 'Confirmar contraseña',
				'submit' : 'Enviar',

			/*profile*/
				'Email Address' : 'Dirección de correo electrónico',
				'Address' : 'Dirección',
				'CHANGE PASSWORD' : 'CAMBIA LA CONTRASEÑA',

			/*rate student*/
				'Actitud_Ability' : 'Capacidad de Aptitud',
				'Force_Skill' : 'Habilidad de fuerza',
				'Puntuality' : 'Puntualidad',

			/*rate teacher*/
				'Subject' : 'Tema',
				'Patience' : 'Paciencia',
				'Explanation' : 'Explicación',
				'Punctual' : 'Puntual',
				'Ability' : 'Capacidad',

			/*schedule class*/
			'Set Time' : 'Fijar tiempo',
			'Update' : 'Actualizar'	,
			'forget' : 'Olvidar',
			'Change Password' : 'Cambia la contraseña',
			'Detail' : 'Detalle',
			'Rate The Student' : 'Clasifique al estudiante',
			'Rate The Teacher' : 'Califica al Maestro',
			'course_required_error' : 'Nombre del curso requerido',
  });
 
  $translateProvider.preferredLanguage('en');
}]);